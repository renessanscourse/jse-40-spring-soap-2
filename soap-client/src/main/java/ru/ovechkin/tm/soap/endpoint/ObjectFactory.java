
package ru.ovechkin.tm.soap.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ovechkin.tm.soap.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindAll_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "findAll");
    private final static QName _FindAllResponse_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "findAllResponse");
    private final static QName _FindById_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "findById");
    private final static QName _FindByIdResponse_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "findByIdResponse");
    private final static QName _RemoveById_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "removeById");
    private final static QName _RemoveByIdResponse_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "removeByIdResponse");
    private final static QName _Save_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "save");
    private final static QName _SaveResponse_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "saveResponse");
    private final static QName _UpdateById_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "updateById");
    private final static QName _UpdateByIdResponse_QNAME = new QName("http://endpoint.soap.tm.ovechkin.ru/", "updateByIdResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ovechkin.tm.soap.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindById }
     * 
     */
    public FindById createFindById() {
        return new FindById();
    }

    /**
     * Create an instance of {@link FindByIdResponse }
     * 
     */
    public FindByIdResponse createFindByIdResponse() {
        return new FindByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveById }
     * 
     */
    public RemoveById createRemoveById() {
        return new RemoveById();
    }

    /**
     * Create an instance of {@link RemoveByIdResponse }
     * 
     */
    public RemoveByIdResponse createRemoveByIdResponse() {
        return new RemoveByIdResponse();
    }

    /**
     * Create an instance of {@link Save }
     * 
     */
    public Save createSave() {
        return new Save();
    }

    /**
     * Create an instance of {@link SaveResponse }
     * 
     */
    public SaveResponse createSaveResponse() {
        return new SaveResponse();
    }

    /**
     * Create an instance of {@link UpdateById }
     * 
     */
    public UpdateById createUpdateById() {
        return new UpdateById();
    }

    /**
     * Create an instance of {@link UpdateByIdResponse }
     * 
     */
    public UpdateByIdResponse createUpdateByIdResponse() {
        return new UpdateByIdResponse();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "findById")
    public JAXBElement<FindById> createFindById(FindById value) {
        return new JAXBElement<FindById>(_FindById_QNAME, FindById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "findByIdResponse")
    public JAXBElement<FindByIdResponse> createFindByIdResponse(FindByIdResponse value) {
        return new JAXBElement<FindByIdResponse>(_FindByIdResponse_QNAME, FindByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "removeById")
    public JAXBElement<RemoveById> createRemoveById(RemoveById value) {
        return new JAXBElement<RemoveById>(_RemoveById_QNAME, RemoveById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "removeByIdResponse")
    public JAXBElement<RemoveByIdResponse> createRemoveByIdResponse(RemoveByIdResponse value) {
        return new JAXBElement<RemoveByIdResponse>(_RemoveByIdResponse_QNAME, RemoveByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Save }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "save")
    public JAXBElement<Save> createSave(Save value) {
        return new JAXBElement<Save>(_Save_QNAME, Save.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "saveResponse")
    public JAXBElement<SaveResponse> createSaveResponse(SaveResponse value) {
        return new JAXBElement<SaveResponse>(_SaveResponse_QNAME, SaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "updateById")
    public JAXBElement<UpdateById> createUpdateById(UpdateById value) {
        return new JAXBElement<UpdateById>(_UpdateById_QNAME, UpdateById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "updateByIdResponse")
    public JAXBElement<UpdateByIdResponse> createUpdateByIdResponse(UpdateByIdResponse value) {
        return new JAXBElement<UpdateByIdResponse>(_UpdateByIdResponse_QNAME, UpdateByIdResponse.class, null, value);
    }

}
