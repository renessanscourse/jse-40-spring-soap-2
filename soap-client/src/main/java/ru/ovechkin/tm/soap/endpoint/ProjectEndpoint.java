package ru.ovechkin.tm.soap.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-16T11:56:16.775+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @RequestWrapper(localName = "findById", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.FindById")
    @ResponseWrapper(localName = "findByIdResponse", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.FindByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.ovechkin.tm.soap.endpoint.Project findById(
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @RequestWrapper(localName = "updateById", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.UpdateById")
    @ResponseWrapper(localName = "updateByIdResponse", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.UpdateByIdResponse")
    public void updateById(
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "project", targetNamespace = "")
        ru.ovechkin.tm.soap.endpoint.Project project
    );

    @WebMethod
    @RequestWrapper(localName = "removeById", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.RemoveById")
    @ResponseWrapper(localName = "removeByIdResponse", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.RemoveByIdResponse")
    public void removeById(
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @RequestWrapper(localName = "findAll", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.FindAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.ovechkin.tm.soap.endpoint.Project> findAll();

    @WebMethod
    @RequestWrapper(localName = "save", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.Save")
    @ResponseWrapper(localName = "saveResponse", targetNamespace = "http://endpoint.soap.tm.ovechkin.ru/", className = "ru.ovechkin.tm.soap.endpoint.SaveResponse")
    public void save(
        @WebParam(name = "project", targetNamespace = "")
        ru.ovechkin.tm.soap.endpoint.Project project
    );
}
