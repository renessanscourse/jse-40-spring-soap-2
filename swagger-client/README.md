# swagger-java-client

Swagger Maven Plugin Sample
- API version: v1
  - Build date: 2021-02-12T17:20:02.224+03:00

This is a sample for swagger-maven-plugin

  For more information, please visit [http://www.ovechkin.ru](http://www.ovechkin.ru)

*Automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen)*


## Requirements

Building the API client library requires:
1. Java 1.7+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>io.swagger</groupId>
  <artifactId>swagger-java-client</artifactId>
  <version>1.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/swagger-java-client-1.0.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.DefaultApi;

import java.io.File;
import java.util.*;

public class DefaultApiExample {

    public static void main(String[] args) {
        
        DefaultApi apiInstance = new DefaultApi();
        try {
            List<Project> result = apiInstance.allProjects();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#allProjects");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**allProjects**](docs/DefaultApi.md#allProjects) | **GET** /rest/projects/all | 
*DefaultApi* | [**create**](docs/DefaultApi.md#create) | **PUT** /rest/projects/create | 
*DefaultApi* | [**create_0**](docs/DefaultApi.md#create_0) | **POST** /rest/tasks/create | 
*DefaultApi* | [**edit**](docs/DefaultApi.md#edit) | **POST** /rest/projects/edit | 
*DefaultApi* | [**edit_0**](docs/DefaultApi.md#edit_0) | **PATCH** /rest/tasks/edit | 
*DefaultApi* | [**remove**](docs/DefaultApi.md#remove) | **DELETE** /rest/projects/remove | 
*DefaultApi* | [**remove_0**](docs/DefaultApi.md#remove_0) | **DELETE** /rest/tasks/remove | 
*DefaultApi* | [**show**](docs/DefaultApi.md#show) | **GET** /rest/tasks/{projectId} | 


## Documentation for Models

 - [Project](docs/Project.md)
 - [Task](docs/Task.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

ovechkin@roman.ru

